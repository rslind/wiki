# Advancement Rules
Here you can find some more explanation about the rule that you can't tell anyone about how or where to get an advancement.

In general, you can't tell anyone how to complete an advancement, like "oh yeah Nathan's diary is at (these coordinates)" or "Just look in (this building)"

## Working together
If you and your friends want to complete an advancement together, you're allowed to do so **IF** none of you have completed this advancement before (you can't lead someone to the advancement).

Additionally, you can not divide the work. You have to search as one unit. You can't tell your friends to go look in place A while you look in place B. In short: **you have to stay together** and if one of your friends finds something on accident when they're solo, they can not tell you where it is.

## Exceptions

### Gameplay
**Rule of thumb: you can explain gameplay to other players.**

This means that you can tell people how to do gameplay actions. These actions may lead to an advancement, so these are exempt from the rule. 

Find the list [here](#List)
### Co-op advancements
There's also an exception for advancements that need more than one player to be completed.

You can, of course, ask other players to join you for these advancements. You are encouraged to not spoil the precise nature of these advancements though, to keep it interesting for others.

### List
This is the full list of advancements you're allowed to explain, for being either gameplay related or co-op.

**This list may be outdated, so ask an admin when in doubt.**
- Kadic
    * Are you going to finish that?
    * Gravy... It needs more gravy
    * Lamb and potatoes for dessert
    * The Goodest Boy
    * Windows lover
    * R.I.P.
    * But why is it gray?
- Factory
    * Déjà Vu
- Lyoko
    * All of them (in the Lyoko tab)

**Note that none of the Sector 5 advancements are exempt**


## Punishments
If you are found helping others with advancements one of the following things can happen; you may be:
* Warned
* Excluded from getting Veteran status
* Banned temporarily
* Banned permanently

Usually these punishments will be in that order, unless you keep intentionally breaking the rules.