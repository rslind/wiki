[English](/Rules/ChannelUsage.md) | [Français](/Translated/French/Rules/ChannelUsage.md) | [Romanian](/Translated/Romanian/Rules/ChannelUsage.md)

---
Please help us translate this page!
Contact us on discord for help on how to work with git, or submit a merge request if you know how!